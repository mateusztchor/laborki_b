package pk.labs.LabB;

import pk.labs.LabB.Implementation.*;
import pk.labs.LabB.Contracts.*;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = pk.labs.LabB.Implementation.DisplayClass.class.getName();
    public static String controlPanelImplClassName = pk.labs.LabB.Implementation.ControlPanelClass.class.getName();

    public static String mainComponentSpecClassName = pk.labs.LabB.Contracts.MainInterface.class.getName();
    public static String mainComponentImplClassName = pk.labs.LabB.Implementation.MainClass.class.getName();
    public static String mainComponentBeanName = "mainClass";
    // endregion

    // region P2
    public static String mainComponentMethodName = "..";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "...";
    // endregion
}
